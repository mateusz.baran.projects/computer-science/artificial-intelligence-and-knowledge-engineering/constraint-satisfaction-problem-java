import csp.Backtracking;
import csp.CSP;
import csp.ForwardChecking;
import heuristic.Heuristic;
import heuristic.value.AscendingValues;
import heuristic.value.LeastConstrainingValue;
import heuristic.value.ValueHeuristic;
import heuristic.variable.*;
import model.Futoshiki;
import model.Model;

public class RunFutoshiki {
    public static void main(String[] args) {

        String directory = "src/data/";

        String fileName = "test_futo_6_1.txt";
//        String fileName = "futoshiki_5_4.txt";


        Model<Integer> futoshiki = new Futoshiki(directory + fileName).load();

//        VariableHeuristic<Integer> variableHeuristic = new RandomOrderVariable<>();
//        VariableHeuristic<Integer> variableHeuristic = new MostConstrainingVariable<>();
//        VariableHeuristic<Integer> variableHeuristic = new MostConstrainedVariable<>();
        VariableHeuristic<Integer> variableHeuristic = new InOrderVariable<>();

//        ValueHeuristic<Integer> valueHeuristic = new AscendingValues<>();
        ValueHeuristic<Integer> valueHeuristic = new LeastConstrainingValue<>();

        Heuristic<Integer> heuristic = new Heuristic<>(futoshiki, valueHeuristic, variableHeuristic);

        CSP<Integer> csp = new ForwardChecking<>(futoshiki, heuristic);

        System.out.println("Model: " + fileName);
        csp.setFindAllSolutions(false);
        csp.run();

        for (int i = 0; i < csp.getSolutions().size(); i++) {
            csp = csp.getSolutions().get(i);
            System.out.println("Futoshiki - solution - " + (i + 1));
            System.out.println(csp.getModel());
            System.out.println(csp.getModel().checkConstraints());

            System.out.println("Futoshiki");
            System.out.println("File: " + fileName);
            System.out.println("Possible combinations: " + csp.countAllCombinations());
            System.out.println("Iterations: " + csp.getIterations());
            System.out.println("Backs: " + csp.getBacktracks());
            System.out.println("Solutions: " + csp.getSolutions().size());
            System.out.println("Time: " + csp.getTime());
        }

        if (csp.getSolutions().isEmpty()) {
            System.out.println("Futoshiki");
            System.out.println(csp.getModel());
            System.out.println("File: " + fileName);
            System.out.println("Possible combinations: " + csp.countAllCombinations());
            System.out.println("Iterations: " + csp.getIterations());
            System.out.println("Backs: " + csp.getBacktracks());
            System.out.println("Solutions: " + csp.getSolutions().size());
            System.out.println("Time: " + csp.getTime());
        }
    }
}
