import csp.Backtracking;
import csp.CSP;
import csp.ForwardChecking;
import heuristic.Heuristic;
import heuristic.value.AscendingValues;
import heuristic.value.LeastConstrainingValue;
import heuristic.value.ValueHeuristic;
import heuristic.variable.*;
import model.Futoshiki;
import model.Model;
import model.Skyscrapper;
import utils.Copy;
import utils.SaverCSV;

import java.util.HashMap;

public class RunAll {
    public static void main(String[] args) {

        String directory = "src/data/";

        SaverCSV saverCSV = new SaverCSV("datasheets/results.csv");

        HashMap<Boolean, String> findAllSolutions = new HashMap<>();
        findAllSolutions.put(true, "true");
        findAllSolutions.put(false, "false");

        HashMap<Boolean, String> algorithms = new HashMap<>();
        algorithms.put(true, "Forward Checking");
        algorithms.put(false, "Backtracking");

        HashMap<Model<Integer>, String> models = new HashMap<>();
        models.put(new Futoshiki(directory + "test_futo_4_0.txt").load(), "test_futo_4_0");
        models.put(new Futoshiki(directory + "test_futo_4_1.txt").load(), "test_futo_4_1");
        models.put(new Futoshiki(directory + "test_futo_4_2.txt").load(), "test_futo_4_2");
        models.put(new Futoshiki(directory + "test_futo_5_0.txt").load(), "test_futo_5_0");
        models.put(new Futoshiki(directory + "test_futo_5_1.txt").load(), "test_futo_5_1");
        models.put(new Futoshiki(directory + "test_futo_5_2.txt").load(), "test_futo_5_2");
        models.put(new Futoshiki(directory + "test_futo_6_0.txt").load(), "test_futo_6_0");
        models.put(new Futoshiki(directory + "test_futo_6_1.txt").load(), "test_futo_6_1");
        models.put(new Futoshiki(directory + "test_futo_6_2.txt").load(), "test_futo_6_2");
        models.put(new Futoshiki(directory + "test_futo_7_0.txt").load(), "test_futo_7_0");
        models.put(new Futoshiki(directory + "test_futo_7_1.txt").load(), "test_futo_7_1");
        models.put(new Futoshiki(directory + "test_futo_7_2.txt").load(), "test_futo_7_2");
        models.put(new Futoshiki(directory + "test_futo_8_0.txt").load(), "test_futo_8_0");
        models.put(new Futoshiki(directory + "test_futo_8_1.txt").load(), "test_futo_8_1");
        models.put(new Futoshiki(directory + "test_futo_8_2.txt").load(), "test_futo_8_2");
        models.put(new Futoshiki(directory + "test_futo_9_0.txt").load(), "test_futo_9_0");
        models.put(new Futoshiki(directory + "test_futo_9_1.txt").load(), "test_futo_9_1");
        models.put(new Futoshiki(directory + "test_futo_9_2.txt").load(), "test_futo_9_2");
        models.put(new Skyscrapper(directory + "test_sky_4_0.txt").load(), "test_sky_4_0");
        models.put(new Skyscrapper(directory + "test_sky_4_1.txt").load(), "test_sky_4_1");
        models.put(new Skyscrapper(directory + "test_sky_4_2.txt").load(), "test_sky_4_2");
        models.put(new Skyscrapper(directory + "test_sky_4_3.txt").load(), "test_sky_4_3");
        models.put(new Skyscrapper(directory + "test_sky_4_4.txt").load(), "test_sky_4_4");
        models.put(new Skyscrapper(directory + "test_sky_5_0.txt").load(), "test_sky_5_0");
        models.put(new Skyscrapper(directory + "test_sky_5_1.txt").load(), "test_sky_5_1");
        models.put(new Skyscrapper(directory + "test_sky_5_2.txt").load(), "test_sky_5_2");
        models.put(new Skyscrapper(directory + "test_sky_5_3.txt").load(), "test_sky_5_3");
        models.put(new Skyscrapper(directory + "test_sky_5_4.txt").load(), "test_sky_5_4");
        models.put(new Skyscrapper(directory + "test_sky_6_0.txt").load(), "test_sky_6_0");
        models.put(new Skyscrapper(directory + "test_sky_6_1.txt").load(), "test_sky_6_1");
        models.put(new Skyscrapper(directory + "test_sky_6_2.txt").load(), "test_sky_6_2");
        models.put(new Skyscrapper(directory + "test_sky_6_3.txt").load(), "test_sky_6_3");
        models.put(new Skyscrapper(directory + "test_sky_6_4.txt").load(), "test_sky_6_4");

        HashMap<VariableHeuristic<Integer>, String> variableHeuristics = new HashMap<>();
        variableHeuristics.put(new InOrderVariable<>(), "In Order Variable");
        variableHeuristics.put(new MostConstrainingVariable<>(), "Most Constraining Variable");
        variableHeuristics.put(new MostConstrainedVariable<>(), "Most Constrained Variable");
        variableHeuristics.put(new RandomOrderVariable<>(), "Random Order Variable");

        HashMap<ValueHeuristic<Integer>, String> valueHeuristics = new HashMap<>();
        valueHeuristics.put(new AscendingValues<>(), "Ascending Values");
        valueHeuristics.put(new LeastConstrainingValue<>(), "Least Constraining Value");


        int progress = 1;

        saverCSV.add("Model");
        saverCSV.add("Algorithm");
        saverCSV.add("Variable Heuristic");
        saverCSV.add("Value Heuristic");
        saverCSV.add("Iterations");
        saverCSV.add("Backtracks");
        saverCSV.add("Time");
        saverCSV.add("Find All Solutions");
        saverCSV.add("Solutions");
        saverCSV.addLastInRow("All Combinations");

        for (Boolean isForwardChecking : algorithms.keySet()) {
            for (Boolean findAllSolutionsFlag : findAllSolutions.keySet()) {
                for (VariableHeuristic<Integer> variableHeuristic : variableHeuristics.keySet()) {
                    for (ValueHeuristic<Integer> valueHeuristic : valueHeuristics.keySet()) {
                        for (Model<Integer> model : models.keySet()) {
                            Model<Integer> modelTemp = (Model<Integer>) Copy.deepCopy(model);
                            Heuristic<Integer> heuristic = new Heuristic<>(modelTemp, valueHeuristic, variableHeuristic);

                            CSP<Integer> csp;
                            if (isForwardChecking) csp = new ForwardChecking<>(modelTemp, heuristic);
                            else csp = new Backtracking<>(modelTemp, heuristic);

                            int allCombinations = findAllSolutions.size();
                            allCombinations *= models.size();
                            allCombinations *= variableHeuristics.size();
                            allCombinations *= valueHeuristics.size();
                            allCombinations *= algorithms.size();


                            System.out.println(String.format("Progress %d/%d", progress, allCombinations));
                            System.out.println("Model: " + models.get(model));

                            csp.setFindAllSolutions(findAllSolutionsFlag);
                            csp.run();
                            System.out.println("Algorithm: " + algorithms.get(isForwardChecking));
                            System.out.println("Variable Heuristic: " + variableHeuristics.get(variableHeuristic));
                            System.out.println("Value Heuristic: " + valueHeuristics.get(valueHeuristic));
                            System.out.println("Iterations: " + csp.getIterations());
                            System.out.println("Backtracks: " + csp.getBacktracks());
                            System.out.println(String.format("Time: %f", csp.getTime()));
                            System.out.println("Find All Solutions: " + findAllSolutions.get(findAllSolutionsFlag));
                            System.out.println("Solutions: " + csp.getSolutions().size());
                            System.out.println(String.format("All Combinations: %.0f", csp.countAllCombinations()));
                            System.out.println();

                            saverCSV.add(models.get(model));
                            saverCSV.add(algorithms.get(isForwardChecking));
                            saverCSV.add(variableHeuristics.get(variableHeuristic));
                            saverCSV.add(valueHeuristics.get(valueHeuristic));
                            saverCSV.add(String.valueOf(csp.getIterations()));
                            saverCSV.add(String.valueOf(csp.getBacktracks()));
                            saverCSV.add(String.format("%f", csp.getTime()));
                            saverCSV.add(findAllSolutions.get(findAllSolutionsFlag));
                            saverCSV.add(String.valueOf(csp.getSolutions().size()));
                            saverCSV.addLastInRow(String.format("%.0f", csp.countAllCombinations()));

                            progress += 1;
                        }
                    }
                }
            }
        }

        saverCSV.save();
    }
}
