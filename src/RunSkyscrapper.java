import csp.Backtracking;
import csp.Backtracking;
import csp.CSP;
import heuristic.Heuristic;
import heuristic.value.AscendingValues;
import heuristic.value.LeastConstrainingValue;
import heuristic.value.ValueHeuristic;
import heuristic.variable.*;
import model.Skyscrapper;

public class RunSkyscrapper {
    public static void main(String[] args) {

        String directory = "src/data/";

        String fileName = "test_sky_6_2.txt";
//        String fileName = "skyscrapper_5_4.txt";
//        String fileName = "skyscrapper_4_0.txt";

        Skyscrapper skyscrapper = new Skyscrapper(directory + fileName);
        skyscrapper.load();

//        VariableHeuristic<Integer> variableHeuristic = new RandomOrderVariable<>();
//        VariableHeuristic<Integer> variableHeuristic = new MostConstrainingVariable<>();
        VariableHeuristic<Integer> variableHeuristic = new MostConstrainedVariable<>();
//        VariableHeuristic<Integer> variableHeuristic = new InOrderVariable<>();

        ValueHeuristic<Integer> valueHeuristic = new AscendingValues<>();
//        ValueHeuristic valueHeuristic = new LeastConstrainingValue<>();

        Heuristic<Integer> heuristic = new Heuristic<>(skyscrapper, valueHeuristic, variableHeuristic);

        CSP<Integer> csp = new Backtracking<>(skyscrapper, heuristic);

        System.out.println("Model: " + fileName);
        csp.setFindAllSolutions(false);
        csp.run();

        for (int i = 0; i < csp.getSolutions().size(); i++) {
            csp = csp.getSolutions().get(i);
            System.out.println("Skyscrapper - solution - " + (i + 1));
            System.out.println(csp.getModel());
            System.out.println(csp.getModel().checkConstraints());

            System.out.println("Skyscrapper");
            System.out.println("File: " + fileName);
            System.out.println("Possible combinations: " + csp.countAllCombinations());
            System.out.println("Iterations: " + csp.getIterations());
            System.out.println("Backs: " + csp.getBacktracks());
            System.out.println("Solutions: " + csp.getSolutions().size());
            System.out.println("Time: " + csp.getTime());
        }

        if (csp.getSolutions().isEmpty()) {
            System.out.println("Skyscrapper");
            System.out.println(csp.getModel());
            System.out.println("File: " + fileName);
            System.out.println("Possible combinations: " + csp.countAllCombinations());
            System.out.println("Iterations: " + csp.getIterations());
            System.out.println("Backs: " + csp.getBacktracks());
            System.out.println("Solutions: " + csp.getSolutions().size());
            System.out.println("Time: " + csp.getTime());
        }
    }
}
