package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class SaverCSV {

    private String path;
    private StringBuilder sb;

    public SaverCSV(String path) {
        this.path = path;
        this.sb = new StringBuilder();
    }

    public void save() {
        try (PrintWriter writer = new PrintWriter(new File(path))) {

            writer.write(sb.toString());
            System.out.println("Saved " + path);

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    public void add(String value) {
        sb.append(String.format("\"%s\", ", value));
    }

    public void addLastInRow(String value) {
        sb.append(String.format("\"%s\"\n", value));
    }
}
