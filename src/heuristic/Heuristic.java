package heuristic;

import heuristic.value.ValueHeuristic;
import heuristic.variable.VariableHeuristic;
import model.Model;
import model.variable.Variable;

import java.io.Serializable;
import java.util.List;

public class Heuristic<T> implements Serializable {
    private ValueHeuristic<T> valueHeuristic;
    private VariableHeuristic<T> variableHeuristic;

    public Heuristic(Model<T> model, ValueHeuristic<T> valueHeuristic, VariableHeuristic<T> variableHeuristic) {
        this.valueHeuristic = valueHeuristic;
        this.variableHeuristic = variableHeuristic;

        variableHeuristic.init(model);
        valueHeuristic.init(model, variableHeuristic);
    }

    public List<Variable<T>> getVariables() {
        return variableHeuristic.getVariables();
    }

    public Variable<T> getVariable(Integer index) {
        return variableHeuristic.getVariable(index);
    }

    public List<T> getSortedDomainValues(Variable<T> variable) {
        return valueHeuristic.getSortedDomainValues(variable);
    }
}
