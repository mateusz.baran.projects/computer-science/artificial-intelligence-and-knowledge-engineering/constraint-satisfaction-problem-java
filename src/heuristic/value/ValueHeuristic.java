package heuristic.value;

import heuristic.variable.VariableHeuristic;
import model.Model;
import model.variable.Variable;

import java.io.Serializable;
import java.util.List;

public abstract class ValueHeuristic<T> implements Serializable {
    public abstract void init(Model<T> model, VariableHeuristic<T> variableHeuristic);

    public abstract List<T> getSortedDomainValues(Variable<T> variable);
}
