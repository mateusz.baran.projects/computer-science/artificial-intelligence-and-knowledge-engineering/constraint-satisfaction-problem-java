package heuristic.value;

import heuristic.variable.VariableHeuristic;
import model.Model;
import model.variable.Variable;

import java.util.*;

public class LeastConstrainingValue<T> extends ValueHeuristic<T> {

    protected HashMap<Variable<T>, Set<Variable<T>>> constrainedVariables;
    private Model<T> model;

    public LeastConstrainingValue() { }

    @Override
    public void init(Model<T> model, VariableHeuristic<T> variableHeuristic) {
        this.model = model;

        this.constrainedVariables = new HashMap<>();
        for (Variable<T> variable : model.getConstrainedVariables().keySet())
            constrainedVariables.put(variable, new HashSet<>(model.getConstrainedVariables().get(variable)));
    }

    @Override
    public List<T> getSortedDomainValues(Variable<T> variable) {
        List<Integer> availableValues = new ArrayList<>();
        List<T> domainValues = variable.getLeftDomainValues();

        for (T value : domainValues) {
            variable.setValue(value);
            availableValues.add(countAvailableValues(variable));
        }

        variable.resetDomain();

        sort(domainValues, availableValues);

        return domainValues;
    }

    private int countAvailableValues(Variable<T> var) {
        int sum = 0;

        for (Variable<T> variable: constrainedVariables.get(var)) {
            if (variable.isNull()) {
                for (T value : variable.getLeftDomainValues()) {
                    variable.setValue(value);
                    if (model.validate(variable))
                        sum += 1;
                }

                variable.resetDomain();
            }
        }

        return sum;
    }

    private void sort(List<T> array, List<Integer> order) {
        for (int i = 0; i < array.size() - 1; i++) {
            for (int j = i + 1; j < array.size(); j++) {
                if (order.get(i) < order.get(j)){
                    replace(array, i, j);
                    replaceInteger(order, i, j);
                }
            }
        }
    }

    private void replace(List<T> array, Integer indexFirst, Integer indexSecond) {
        T temp = array.get(indexFirst);
        array.set(indexFirst, array.get(indexSecond));
        array.set(indexSecond, temp);
    }

    private void replaceInteger(List<Integer> array, Integer indexFirst, Integer indexSecond) {
        Integer temp = array.get(indexFirst);
        array.set(indexFirst, array.get(indexSecond));
        array.set(indexSecond, temp);
    }
}
