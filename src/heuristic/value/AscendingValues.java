package heuristic.value;

import heuristic.variable.VariableHeuristic;
import model.Model;
import model.variable.Variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AscendingValues<T> extends ValueHeuristic<T> {

    public AscendingValues() {
    }

    @Override
    public void init(Model<T> model, VariableHeuristic<T> variableHeuristic) {
    }

    @Override
    public List<T> getSortedDomainValues(Variable<T> variable) {
        return variable.getLeftDomainValues().stream().sorted().collect(Collectors.toList());
    }
}
