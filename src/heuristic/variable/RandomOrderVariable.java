package heuristic.variable;

import model.Model;
import model.variable.Variable;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class RandomOrderVariable<T> extends VariableHeuristic<T> {

    private List<Variable<T>> variables;

    public RandomOrderVariable() { }

    @Override
    public void init(Model<T> model) {
        this.variables = model.getVariables().stream().filter(Variable::isNull).collect(Collectors.toList());
        Collections.shuffle(variables);
    }

    @Override
    public List<Variable<T>> getVariables() {
        return variables;
    }

    @Override
    public Variable<T> getVariable(Integer index) {
        return variables.get(index);
    }
}
