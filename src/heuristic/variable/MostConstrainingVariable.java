package heuristic.variable;

import model.Model;
import model.variable.Variable;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class MostConstrainingVariable<T> extends VariableHeuristic<T> {

    private HashMap<Variable<T>, List<Variable<T>>> constrainedVariables;
    private List<Variable<T>> usedVariables;
    private List<Variable<T>> variables;

    public MostConstrainingVariable() { }

    @Override
    public void init(Model<T> model) {
        this.variables = model.getVariables().stream().filter(Variable::isNull).collect(Collectors.toList());
        this.constrainedVariables = model.getConstrainedVariables();
        this.usedVariables = model.getVariables().stream().filter(x -> !x.isNull()).collect(Collectors.toList());

        sortVariables();
    }

    private void sortVariables() {
        for (int i=0; i < variables.size(); i++) {
            Integer bestIndex = indexOfMostConstrainingVariable(i);

            if (!bestIndex.equals(i))
                replace(i, bestIndex);

            usedVariables.add(variables.get(i));
        }
    }

    @Override
    public List<Variable<T>> getVariables() {
        return variables;
    }

    @Override
    public Variable<T> getVariable(Integer index) {
        return variables.get(index);
    }

    private Integer indexOfMostConstrainingVariable(Integer index) {
        int bestSum = 0;

        for (int i = index; i < variables.size(); i++) {
            int sum = 0;

            for (Variable<T> var : constrainedVariables.get(variables.get(i)))
                if (usedVariables.contains(var))
                    sum += countConstrainedVariable(var, variables.get(i));

            if (sum > bestSum) {
                index = i;
                bestSum = sum;
            }
        }

        return index;
    }

    private int countConstrainedVariable(Variable<T> constraining, Variable<T> constrained) {
        int sum = 0;
        for (Variable<T> var : constrainedVariables.get(constraining))
            if (var == constrained)
                sum += 1;

        return sum;
    }

    private void replace(Integer indexFirst, Integer indexSecond) {
        Variable<T> temp = variables.get(indexFirst);
        variables.set(indexFirst, variables.get(indexSecond));
        variables.set(indexSecond, temp);
    }
}
