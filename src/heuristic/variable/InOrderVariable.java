package heuristic.variable;

import model.Model;
import model.variable.Variable;

import java.util.List;
import java.util.stream.Collectors;


public class InOrderVariable<T> extends VariableHeuristic<T> {

    private List<Variable<T>> variables;

    public InOrderVariable() { }

    @Override
    public void init(Model<T> model) {
        this.variables = model.getVariables().stream().filter(Variable::isNull).collect(Collectors.toList());
    }

    @Override
    public List<Variable<T>> getVariables() {
        return variables;
    }

    @Override
    public Variable<T> getVariable(Integer index) {
        return variables.get(index);
    }
}
