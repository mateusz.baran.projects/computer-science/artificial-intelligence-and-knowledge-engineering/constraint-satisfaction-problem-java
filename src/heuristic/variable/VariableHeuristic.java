package heuristic.variable;

import model.Model;
import model.variable.Variable;

import java.io.Serializable;
import java.util.List;

public abstract class VariableHeuristic<T> implements Serializable {
    public abstract void init(Model<T> model);

    public abstract List<Variable<T>> getVariables();

    public abstract Variable<T> getVariable(Integer index);
}
