package heuristic.variable;

import model.Model;
import model.variable.Variable;

import java.util.List;
import java.util.stream.Collectors;

public class MostConstrainedVariable<T> extends VariableHeuristic<T> {

    private List<Variable<T>> variables;

    public MostConstrainedVariable() {
    }

    @Override
    public void init(Model<T> model) {
        this.variables = model.getVariables().stream().filter(Variable::isNull).collect(Collectors.toList());
    }

    @Override
    public List<Variable<T>> getVariables() {
        return variables;
    }

    @Override
    public Variable<T> getVariable(Integer index) {
        Integer bestIndex = indexOfMostConstrainedVariable(index);

        if (!bestIndex.equals(index))
            replace(index, bestIndex);

        return variables.get(index);
    }

    private Integer indexOfMostConstrainedVariable(Integer index) {
        Integer bestIndex = index;
        int bestSize = variables.get(index).getLeftDomainValues().size();

        for (int i = index + 1; i < variables.size(); i++) {
            int size = variables.get(i).getLeftDomainValues().size();
            if (bestSize > size) {
                bestIndex = i;
                bestSize = size;
            }
        }

        return bestIndex;
    }

    private void replace(Integer indexFirst, Integer indexSecond) {
        Variable<T> temp = variables.get(indexFirst);
        variables.set(indexFirst, variables.get(indexSecond));
        variables.set(indexSecond, temp);
    }
}
