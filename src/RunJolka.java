import csp.Backtracking;
import csp.CSP;
import heuristic.Heuristic;
import heuristic.value.AscendingValues;
import heuristic.value.ValueHeuristic;
import heuristic.variable.InOrderVariable;
import heuristic.variable.MostConstrainedVariable;
import heuristic.variable.VariableHeuristic;
import model.Jolka;
import model.Model;

public class RunJolka {
    public static void main(String[] args) {

        String directory = "src/data/";

        String fileName = "jolka1";

        Model<String> jolka = new Jolka(directory + fileName).load();

//        VariableHeuristic<String> variableHeuristic = new RandomOrderVariable<>();
//        VariableHeuristic<String> variableHeuristic = new MostConstrainingVariable<>();
//        VariableHeuristic<String> variableHeuristic = new MostConstrainedVariable<>();
        VariableHeuristic<String> variableHeuristic = new InOrderVariable<>();

        ValueHeuristic<String> valueHeuristic = new AscendingValues<>();
//        ValueHeuristic<String> valueHeuristic = new LeastConstrainingValue<>();

        Heuristic<String> heuristic = new Heuristic<>(jolka, valueHeuristic, variableHeuristic);

        CSP<String> csp = new Backtracking<>(jolka, heuristic);

        System.out.println("Model: " + fileName);
        csp.setFindAllSolutions(false);
        csp.run();

        for (int i = 0; i < csp.getSolutions().size(); i++) {
            csp = csp.getSolutions().get(i);
            System.out.println("Jolka - solution - " + (i + 1));
            System.out.println(csp.getModel());
            System.out.println(csp.getModel().checkConstraints());

            System.out.println("Jolka");
            System.out.println("File: " + fileName);
            System.out.println("Possible combinations: " + csp.countAllCombinations());
            System.out.println("Iterations: " + csp.getIterations());
            System.out.println("Backs: " + csp.getBacktracks());
            System.out.println("Solutions: " + csp.getSolutions().size());
            System.out.println("Time: " + csp.getTime());
        }

        if (csp.getSolutions().isEmpty()) {
            System.out.println("Jolka");
            System.out.println(csp.getModel());
            System.out.println("File: " + fileName);
            System.out.println("Possible combinations: " + csp.countAllCombinations());
            System.out.println("Iterations: " + csp.getIterations());
            System.out.println("Backs: " + csp.getBacktracks());
            System.out.println("Solutions: " + csp.getSolutions().size());
            System.out.println("Time: " + csp.getTime());
        }
    }
}
