package model.constraint;

import model.variable.Variable;

import java.util.List;

public class IsUnique<T> extends Constraint {
    private Variable<T> var;
    private List<Variable<T>> vars;

    public IsUnique(String name, Variable<T> var, List<Variable<T>> vars) {
        super(name);
        this.var = var;
        this.vars = vars;
    }

    public Variable<T> getVar() {
        return var;
    }

    public List<Variable<T>> getVars() {
        return vars;
    }

    @Override
    public boolean check() {
        for (Variable<T> variable: vars)
            if (variable.getValue().equals(var.getValue()))
                return false;
        return true;
    }
}
