package model.constraint;

import model.variable.Variable;

public class IsGreater extends Constraint {
    private Variable<Integer> var1;
    private Variable<Integer> var2;

    public IsGreater(String name, Variable<Integer> var1, Variable<Integer> var2) {
        super(name);
        this.var1 = var1;
        this.var2 = var2;
    }

    public Variable<Integer> getVar1() {
        return var1;
    }

    public Variable<Integer> getVar2() {
        return var2;
    }

    @Override
    public boolean check() {
        return var2.isNull() || var1.getValue() > var2.getValue();
    }
}
