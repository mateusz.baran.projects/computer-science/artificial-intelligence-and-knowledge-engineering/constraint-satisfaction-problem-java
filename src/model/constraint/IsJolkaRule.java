package model.constraint;

import model.variable.Variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class IsJolkaRule extends Constraint {
    private Variable<String> variableFirst;
    private int indexFirst;
    private Variable<String> variableSecond;
    private int indexSecond;

    public IsJolkaRule(String name, Variable<String> variableFirst, int indexFirst, Variable<String> variableSecond,
                       int indexSecond) {
        super(name);
        this.variableFirst = variableFirst;
        this.indexFirst = indexFirst;
        this.variableSecond = variableSecond;
        this.indexSecond = indexSecond;
    }

    @Override
    public boolean check() {
        return variableSecond.isNull()
                || variableFirst.getValue().charAt(indexFirst) == variableSecond.getValue().charAt(indexSecond);
    }
}
