package model.constraint;

import model.variable.Variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class IsSkyscrapperRule extends Constraint {
    private Integer nullValue;
    private Integer maxValue;
    private List<Integer> domainValues;
    private List<Variable<Integer>> variablesLine;
    private Integer size;

    public IsSkyscrapperRule(String name, List<Variable<Integer>> variablesLine, List<Integer> domainValues,
                             Integer nullValue, Integer maxValue) {
        super(name);
        this.variablesLine = variablesLine;
        this.nullValue = nullValue;
        this.domainValues = new ArrayList<>(domainValues);
        Collections.sort(this.domainValues);
        this.maxValue = maxValue;
        this.size = variablesLine.size();
    }

    public List<Variable<Integer>> getVariablesLine() {
        return variablesLine;
    }

    public Integer getMaxValue() {
        return maxValue;
    }

    @Override
    public boolean check() {
        List<Integer> line = variablesLine.stream().map(Variable::getValue).collect(Collectors.toList());
        List<Integer> leftValues = domainValues.stream().filter(x -> !line.contains(x)).collect(Collectors.toList());
        return checkPermutation(line, 0, leftValues);
    }

    private boolean checkPermutation(List<Integer> line, Integer index, List<Integer> leftValues) {
        if (index.equals(size))
            return validate(line);

        if (!line.get(index).equals(nullValue))
            return checkPermutation(line, index + 1, leftValues);

        for (Integer value : leftValues) {
            List<Integer> nLeftValues = leftValues.stream().filter(x -> !x.equals(value)).collect(Collectors.toList());

            line.set(index, value);
            if (checkPermutation(new ArrayList<>(line), index + 1, nLeftValues))
                return true;
        }

        return false;
    }

    private boolean validate(List<Integer> line) {
        Integer maxValueTemp = line.get(0);
        int sum = 1;

        for (int i = 1; i < size; i++) {
            if (maxValueTemp < line.get(i)) {
                maxValueTemp = line.get(i);
                sum += 1;
            }
        }

        return sum == maxValue;
    }
}
