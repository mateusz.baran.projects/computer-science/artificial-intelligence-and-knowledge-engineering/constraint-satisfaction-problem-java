package model.constraint;

import java.io.Serializable;

public abstract class Constraint implements Serializable {
    protected String name;

    public Constraint(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract boolean check();
}
