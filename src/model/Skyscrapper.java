package model;

import model.constraint.*;
import model.variable.Variable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Skyscrapper extends Model<Integer> {
    private Integer size;
    private List<Integer> top;
    private List<Integer> bottom;
    private List<Integer> right;
    private List<Integer> left;

    public Skyscrapper(String path) {
        super(path, 0);
        this.top = new ArrayList<>();
        this.bottom = new ArrayList<>();
        this.right = new ArrayList<>();
        this.left = new ArrayList<>();
    }

    @Override
    protected void dataParser() {
        size = Integer.parseInt(data.get(0));
        List<Integer> domainValues = new ArrayList<>();
        for (int i = 1; i <= size; i++)
            domainValues.add(i);

        for (int i = 1; i <= 4; i++) {
            String[] row = data.get(i).split(";");
            String label = row[0];

            List<String> values = new ArrayList<>(Arrays.asList(row).subList(1, row.length));

            for (String value : values)
                switch (label) {
                    case "G":
                        top.add(Integer.parseInt(value));
                        break;
                    case "D":
                        bottom.add(Integer.parseInt(value));
                        break;
                    case "L":
                        left.add(Integer.parseInt(value));
                        break;
                    case "P":
                        right.add(Integer.parseInt(value));
                        break;
                }
        }

        for (int i = 0; i < size * size; i++)
            variables.add(new Variable<>(String.valueOf(i), nullValue, nullValue, domainValues));


        for (int i = 0; i < variables.size(); i++) {
            Variable<Integer> variable = variables.get(i);
            List<Variable<Integer>> varColRow = new ArrayList<>();
            List<Variable<Integer>> varCol = new ArrayList<>();
            List<Variable<Integer>> varRow = new ArrayList<>();

            for (int j = i % size; j < variables.size(); j += size) {
                if (variables.get(j) != variable)
                    varColRow.add(variables.get(j));
                varCol.add(variables.get(j));
            }

            int index = (i / size) * size;
            for (int j = index; j < index + size; j++) {
                if (variables.get(j) != variable)
                    varColRow.add(variables.get(j));
                varRow.add(variables.get(j));
            }

            addConstraint(variable, varColRow, new IsUnique<>(variable.getName(), variable, varColRow));


            List<Variable<Integer>> varColReverse = new ArrayList<>(varCol);
            List<Variable<Integer>> varRowReverse = new ArrayList<>(varRow);
            Collections.reverse(varColReverse);
            Collections.reverse(varRowReverse);


            Integer nTop = top.get(i % size);
            if (nTop != 0)
                addConstraint(variable, varCol, new IsSkyscrapperRule(variable.getName() + " top", varCol,
                        domainValues, nullValue, nTop));

            Integer nBottom = bottom.get(i % size);
            if (nBottom != 0)
                addConstraint(variable, varCol, new IsSkyscrapperRule(variable.getName() + " bottom", varColReverse,
                        domainValues, nullValue, nBottom));

            Integer nLeft = left.get(i / size);
            if (nLeft != 0)
                addConstraint(variable, varRow, new IsSkyscrapperRule(variable.getName() + " left", varRow,
                        domainValues, nullValue, nLeft));

            Integer nRight = right.get(i / size);
            if (nRight != 0)
                addConstraint(variable, varRow, new IsSkyscrapperRule(variable.getName() + " right", varRowReverse,
                        domainValues, nullValue, nRight));
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("   ");
        for (int i = 0; i < size; i++)
            str.append(String.format(" _%d_", top.get(i)));
        str.append("\n");

        for (int i = 0; i < size; i++) {
            str.append(String.format(" %d ", left.get(i)));
            for (int j = 0; j < size; j++) {
                if (i + 1 == size)
                    str.append(String.format("|_%d_", variables.get(i * size + j).getValue()));
                else
                    str.append(String.format("| %d ", variables.get(i * size + j).getValue()));

            }
            str.append(String.format("| %d \n", right.get(i)));
        }

        str.append("   ");
        for (int i = 0; i < size; i++)
            str.append(String.format("  %d ", bottom.get(i)));
        str.append("\n");

        return str.toString();
    }
}














