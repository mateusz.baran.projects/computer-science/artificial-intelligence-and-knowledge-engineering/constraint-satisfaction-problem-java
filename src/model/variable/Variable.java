package model.variable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Variable<T> implements Serializable {
    private String name;
    private T value;
    private T nullValue;
    private List<T> domainValues;
    private List<T> leftDomainValues;
    private boolean isConstant;

    public Variable(String name, T value, T nullValue, List<T> domainVales) {
        this.name = name;
        this.value = value;
        this.nullValue = nullValue;
        this.domainValues = new ArrayList<>(domainVales);
        this.leftDomainValues = new ArrayList<>(domainVales);
        this.isConstant = !value.equals(nullValue);
    }

    public String getName() {
        return name;
    }

    public T getValue() {
        return value;
    }

    public List<T> getLeftDomainValues() {
        return new ArrayList<>(leftDomainValues);
    }

    public List<T> getDomainValues() {
        return domainValues;
    }

    public void setValue(T value) {
        assert value != nullValue;
        this.value = value;
        this.leftDomainValues.remove(value);
    }

    public void resetDomain() {
        this.value = nullValue;
        this.leftDomainValues = new ArrayList<>(domainValues);
    }

    public boolean isNull() {
        return value.equals(nullValue);
    }

    public boolean isConstant() {
        return isConstant;
    }
}
