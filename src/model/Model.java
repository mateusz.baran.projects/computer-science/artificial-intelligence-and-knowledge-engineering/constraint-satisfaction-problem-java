package model;

import model.constraint.Constraint;
import model.variable.Variable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public abstract class Model<T> implements Serializable {
    protected String path;
    protected List<String> data;
    protected T nullValue;
    protected HashMap<Variable<T>, List<Constraint>> constraints;
    protected HashMap<Variable<T>, List<Variable<T>>> constrainedVariables;
    protected List<Variable<T>> variables;

    public Model(String path, T nullValue) {
        this.path = path;
        this.nullValue = nullValue;
        this.constraints = new HashMap<>();
        this.constrainedVariables = new HashMap<>();
        this.variables = new ArrayList<>();
    }

    protected abstract void dataParser();

    public Model<T> load() {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line = br.readLine();

            data = new ArrayList<>();

            while (line != null) {
                data.add(line);
                line = br.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        dataParser();

        return this;
    }

    public boolean validate(Variable<T> variable) {
        if (constraints.containsKey(variable))
            for (Constraint constraint : constraints.get(variable))
                if (!constraint.check())
                    return false;
        return true;
    }

    public void addConstraint(Variable<T> variable, List<Variable<T>> variablePeers, Constraint constraint) {
        if (!constraints.containsKey(variable)) {
            constraints.put(variable, new ArrayList<>());
            constrainedVariables.put(variable, new ArrayList<>());
        }

        constraints.get(variable).add(constraint);
        constrainedVariables.get(variable).addAll(new HashSet<>(variablePeers));
    }

    public void addConstraint(Variable<T> variable, Variable<T> variablePeer, Constraint constraint) {
        if (!constraints.containsKey(variable)) {
            constraints.put(variable, new ArrayList<>());
            constrainedVariables.put(variable, new ArrayList<>());
        }

        constraints.get(variable).add(constraint);
        constrainedVariables.get(variable).add(variablePeer);
    }

    public T getNullValue() {
        return nullValue;
    }

    public HashMap<Variable<T>, List<Constraint>> getConstraints() {
        return constraints;
    }

    public HashMap<Variable<T>, List<Variable<T>>> getConstrainedVariables() {
        return constrainedVariables;
    }

    public List<Variable<T>> getVariables() {
        return variables;
    }

    public String checkConstraints() {
        StringBuilder str = new StringBuilder();
        for (List<Constraint> constraints : constraints.values())
            for (Constraint constraint : constraints)
                if (!constraint.check())
                    str.append(String.format("%s\n", constraint.getName()));

        return str.toString();
    }
}
