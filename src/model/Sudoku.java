package model;

import model.constraint.IsUnique;
import model.variable.Variable;

import java.util.ArrayList;
import java.util.List;

public class Sudoku extends Model<Integer> {
    private int sudokuID;
    private int size;

    public Sudoku(String path, int sudokuID) {
        super(path, 0);

        this.sudokuID = sudokuID;
        this.size = 9;
    }

    @Override
    protected void dataParser() {
        String[] line = data.get(sudokuID).split(";");
        String numbers = line[2];

        List<Integer> domainValues = new ArrayList<>();
        for (int i = 1; i <= this.size; i++)
            domainValues.add(i);

        for (int i = 0; i < numbers.length(); i++) {
            char c = numbers.charAt(i);
            String name = "Row: " + i / size + 1 + " Col: " + i % size + 1;
            if (c == '.') {
                variables.add(new Variable<>(name, nullValue, nullValue, domainValues));
            } else {
                variables.add(new Variable<>(name, Integer.parseInt(String.valueOf(c)), nullValue, domainValues));
            }
        }

        assert variables.size() == size * size;

        for (int var = 0; var < size * size; var++) {
            Variable<Integer> variable = variables.get(var);
            List<Variable<Integer>> constraintVariables = new ArrayList<>();

            int rowStart = (var / size) * size;
            for (int row = rowStart; row < rowStart + size; row++)
                if (row != var)
                    constraintVariables.add(variables.get(row));

            int colStart = var % size;
            for (int col = colStart; col < size * size; col = col + size)
                if (col != var)
                    constraintVariables.add(variables.get(col));

            int sqrRowStart = ((var / size) / 3) * size * 3;
            int sqrColStart = ((var % size) / 3) * 3;
            for (int col = sqrColStart; col < sqrColStart + size * 3; col = col + size)
                for (int row = sqrRowStart; row < sqrRowStart + size / 3; row++) {
                    Variable<Integer> constraintVariable = variables.get(row + col);
                    if (variable != constraintVariable && !constraintVariables.contains(constraintVariable))
                        constraintVariables.add(constraintVariable);
                }

            addConstraint(variable, constraintVariables, new IsUnique<>(variable.getName(), variable, constraintVariables));
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(" _____________________________ \n");

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (j % (size / 3) == 0) str.append("|");

                int value = variables.get(i * size + j).getValue();

                if (i % (size / 3) == 2) {
                    if (value == 0) str.append("___");
                    else str.append(String.format("_%d_", value));
                } else {
                    if (value == 0) str.append("   ");
                    else str.append(String.format(" %d ", value));
                }


            }
            str.append("|\n");
        }

        return str.toString();
    }
}














