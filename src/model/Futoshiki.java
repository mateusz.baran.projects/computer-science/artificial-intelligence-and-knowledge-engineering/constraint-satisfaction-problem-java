package model;

import model.constraint.IsGreater;
import model.constraint.IsLower;
import model.constraint.IsUnique;
import model.variable.Variable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Futoshiki extends Model<Integer> {
    private static String[] ROW_LABELS = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
    private static String[] COL_LABELS = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

    private Integer size;

    public Futoshiki(String path) {
        super(path, 0);
    }

    @Override
    protected void dataParser() {
        size = Integer.parseInt(data.get(0));
        List<Integer> domainValues = new ArrayList<>();
        for (int i = 1; i <= size; i++)
            domainValues.add(i);

        int row = 0;
        for (int i = 2; i < 2 + size; i++) {
            int col = 0;
            for (String cell : data.get(i).split(";")) {
                variables.add(new Variable<>(String.format("%s%s", ROW_LABELS[row], COL_LABELS[col]), Integer.parseInt(cell), nullValue, domainValues));
                col += 1;
            }
            row += 1;
        }

        for (int i = 3 + size; i < data.size(); i++) {
            String name = data.get(i);

            if (name.contains(";")) {
                String[] coords = name.split(";");

                String coords1 = coords[0];
                String coords2 = coords[1];

                int row1 = Arrays.asList(ROW_LABELS).indexOf(String.valueOf(coords1.charAt(0)));
                int col1 = Arrays.asList(COL_LABELS).indexOf(String.valueOf(coords1.charAt(1)));

                int row2 = Arrays.asList(ROW_LABELS).indexOf(String.valueOf(coords2.charAt(0)));
                int col2 = Arrays.asList(COL_LABELS).indexOf(String.valueOf(coords2.charAt(1)));

                Variable<Integer> var1 = variables.get(row1 * size + col1);
                Variable<Integer> var2 = variables.get(row2 * size + col2);

                addConstraint(var1, var2, new IsLower(name, var1, var2));
                addConstraint(var2, var1, new IsGreater(name, var2, var1));
            }
        }

        for (int i = 0; i < variables.size(); i++) {
            Variable<Integer> variable = variables.get(i);
            List<Variable<Integer>> varColRow = new ArrayList<>();

            for (int j = i % size; j < variables.size(); j += size)
                if (variables.get(j) != variable)
                    varColRow.add(variables.get(j));

            int index = (i / size) * size;
            for (int j = index; j < index + size; j++)
                if (variables.get(j) != variable)
                    varColRow.add(variables.get(j));


            addConstraint(variable, varColRow, new IsUnique<>(variable.getName(), variable, varColRow));
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("   ");
        for (int i = 0; i < size; i++)
            str.append(String.format(" _%s_", COL_LABELS[i]));
        str.append("\n");

        for (int i = 0; i < size; i++) {
            str.append(String.format(" %s ", ROW_LABELS[i]));
            for (int j = 0; j < size; j++)
                str.append(String.format("| %d ", variables.get(i * size + j).getValue()));
            str.append("\n");
        }

        return str.toString();
    }
}














