package model;

import javafx.util.Pair;
import model.constraint.IsJolkaRule;
import model.constraint.IsUnique;
import model.variable.Variable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Jolka extends Model<String> {
    private List<List<String>> board;
    private Map<Pair<Integer, Integer>, Pair<Variable<String>, Integer>> horizontalVariables;
    private Map<Pair<Integer, Integer>, Pair<Variable<String>, Integer>> verticalVariables;

    public Jolka(String path) {
        super(path, "_");
        this.board = new ArrayList<>();
        this.horizontalVariables = new HashMap<>();
        this.verticalVariables = new HashMap<>();
    }

    private void addHorizontalVariable(int h, int w, int length, List<String> domainValues) {
        String name = "Horizontal " + h + " " + w;
        List<String> domain = domainValues.stream().filter(x -> x.length() == length).collect(Collectors.toList());
        Variable<String> variable = new Variable<>(name, nullValue, nullValue, domain);
        variables.add(variable);

        for (int i = 0; i < length; i++) {
            Pair<Integer, Integer> pair = new Pair<>(h, w + i);
            horizontalVariables.put(pair, new Pair<>(variable, i));
        }
    }

    private void addVerticalVariable(int h, int w, int length, List<String> domainValues) {
        String name = "Vertical " + h + " " + w;
        List<String> domain = domainValues.stream().filter(x -> x.length() == length).collect(Collectors.toList());
        Variable<String> variable = new Variable<>(name, nullValue, nullValue, domain);
        variables.add(variable);

        for (int i = 0; i < length; i++) {
            Pair<Integer, Integer> pair = new Pair<>(h + i, w);
            verticalVariables.put(pair, new Pair<>(variable, i));
        }
    }

    @Override
    protected void dataParser() {
        String[] line = data.get(0).split("x");
        int height = Integer.parseInt(line[0]);
        int width = Integer.parseInt(line[1]);

        List<String> domainValues = new ArrayList<>();
        for (int i = height + 1; i < data.size(); i++)
            domainValues.add(data.get(i));

        for (int h = 1; h <= height; h++) {
            List<String> row = new ArrayList<>();
            for (int w = 0; w < width; w++) {
                char c = data.get(h).charAt(w);
                row.add(String.valueOf(c));
            }
            board.add(row);
        }

        for (int h = 0; h < height; h++) {
            int start = 0;
            for (int w = 0; w < width; w++) {
                String s = board.get(h).get(w);
                if (s.equals("#")) {
                    int length = w - start;
                    if (length > 1)
                        addHorizontalVariable(h, start, length, domainValues);

                    start = w + 1;
                } else if (w + 1 == width) {
                    int length = w + 1 - start;
                    if (length > 1)
                        addHorizontalVariable(h, start, length, domainValues);
                }
            }
        }

        for (int w = 0; w < width; w++) {
            int start = 0;
            for (int h = 0; h < height; h++) {
                String s = board.get(h).get(w);
                if (s.equals("#")) {
                    int length = h - start;
                    if (length > 1)
                        addVerticalVariable(start, w, length, domainValues);

                    start = h + 1;
                } else if (h + 1 == height) {
                    int length = h + 1 - start;
                    if (length > 1)
                        addVerticalVariable(start, w, length, domainValues);
                }
            }
        }

        for (Pair<Integer, Integer> horizontalPair : horizontalVariables.keySet())
            for (Pair<Integer, Integer> verticalPair : verticalVariables.keySet())
                if (horizontalPair.equals(verticalPair)) {
                    Pair<Variable<String>, Integer> horizontal = horizontalVariables.get(horizontalPair);
                    Pair<Variable<String>, Integer> vertical = verticalVariables.get(verticalPair);

                    addConstraint(vertical.getKey(), horizontal.getKey(),
                            new IsJolkaRule(horizontal.getKey() + " " + vertical.getKey(),
                                    vertical.getKey(), vertical.getValue(), horizontal.getKey(),
                                    horizontal.getValue()));

                    addConstraint(horizontal.getKey(), vertical.getKey(),
                            new IsJolkaRule(vertical.getKey() + " " + horizontal.getKey(),
                                    horizontal.getKey(), horizontal.getValue(), vertical.getKey(),
                                    vertical.getValue()));
                }

        for (int i = 0; i < variables.size(); i++) {
            List<Variable<String>> vars = new ArrayList<>(variables);
            Variable<String> var = vars.remove(i);
            addConstraint(var, vars, new IsUnique<>(var.getName(), var, vars));
        }
    }

    private char getLetter(int h, int w) {
        Pair<Integer, Integer> pair = new Pair<>(h, w);
        if (horizontalVariables.containsKey(pair)) {
            if (horizontalVariables.get(pair).getKey().isNull())
                return '_';
            return horizontalVariables.get(pair).getKey().getValue().charAt(horizontalVariables.get(pair).getValue());
        } else if (verticalVariables.containsKey(pair)) {
            if (verticalVariables.get(pair).getKey().isNull())
                return '_';
            return verticalVariables.get(pair).getKey().getValue().charAt(verticalVariables.get(pair).getValue());
        }
        return '_';
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("\n");

        for (int h = 0; h < board.size(); h++) {
            for (int w = 0; w < board.get(0).size(); w++) {
                if ("_".equals(board.get(h).get(w)))
                    str.append(getLetter(h, w));
                else
                    str.append("#");
            }
            str.append("\n");
        }

        return str.toString();
    }
}














