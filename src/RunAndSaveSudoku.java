import csp.Backtracking;
import csp.CSP;
import csp.ForwardChecking;
import heuristic.Heuristic;
import heuristic.value.AscendingValues;
import heuristic.value.LeastConstrainingValue;
import heuristic.value.ValueHeuristic;
import heuristic.variable.*;
import model.Model;
import model.Sudoku;
import utils.Copy;
import utils.SaverCSV;

import java.util.HashMap;

public class RunAndSaveSudoku {
    public static void main(String[] args) {

        String directory = "src/data/";

        SaverCSV saverCSV = new SaverCSV("datasheets/sudoku-results.csv");

        HashMap<Boolean, String> findAllSolutions = new HashMap<>();
        findAllSolutions.put(true, "true");
        findAllSolutions.put(false, "false");

        HashMap<Boolean, String> algorithms = new HashMap<>();
        algorithms.put(true, "Forward Checking");
        algorithms.put(false, "Backtracking");

        HashMap<Model<Integer>, String> models = new HashMap<>();
        models.put(new Sudoku(directory + "Sudoku.csv", 1).load(), "sudoku-1");
        models.put(new Sudoku(directory + "Sudoku.csv", 2).load(), "sudoku-2");
        models.put(new Sudoku(directory + "Sudoku.csv", 3).load(), "sudoku-3");
        models.put(new Sudoku(directory + "Sudoku.csv", 4).load(), "sudoku-4");
        models.put(new Sudoku(directory + "Sudoku.csv", 5).load(), "sudoku-5");
        models.put(new Sudoku(directory + "Sudoku.csv", 6).load(), "sudoku-6");
        models.put(new Sudoku(directory + "Sudoku.csv", 7).load(), "sudoku-7");
        models.put(new Sudoku(directory + "Sudoku.csv", 8).load(), "sudoku-8");
        models.put(new Sudoku(directory + "Sudoku.csv", 9).load(), "sudoku-9");
        models.put(new Sudoku(directory + "Sudoku.csv", 10).load(), "sudoku-10");
        models.put(new Sudoku(directory + "Sudoku.csv", 11).load(), "sudoku-11");
        models.put(new Sudoku(directory + "Sudoku.csv", 12).load(), "sudoku-12");
        models.put(new Sudoku(directory + "Sudoku.csv", 13).load(), "sudoku-13");
        models.put(new Sudoku(directory + "Sudoku.csv", 14).load(), "sudoku-14");
        models.put(new Sudoku(directory + "Sudoku.csv", 15).load(), "sudoku-15");
        models.put(new Sudoku(directory + "Sudoku.csv", 16).load(), "sudoku-16");
        models.put(new Sudoku(directory + "Sudoku.csv", 17).load(), "sudoku-17");
        models.put(new Sudoku(directory + "Sudoku.csv", 18).load(), "sudoku-18");
        models.put(new Sudoku(directory + "Sudoku.csv", 19).load(), "sudoku-19");
        models.put(new Sudoku(directory + "Sudoku.csv", 20).load(), "sudoku-20");
        models.put(new Sudoku(directory + "Sudoku.csv", 21).load(), "sudoku-21");
        models.put(new Sudoku(directory + "Sudoku.csv", 22).load(), "sudoku-22");
        models.put(new Sudoku(directory + "Sudoku.csv", 23).load(), "sudoku-23");
        models.put(new Sudoku(directory + "Sudoku.csv", 24).load(), "sudoku-24");
        models.put(new Sudoku(directory + "Sudoku.csv", 25).load(), "sudoku-25");
        models.put(new Sudoku(directory + "Sudoku.csv", 26).load(), "sudoku-26");
        models.put(new Sudoku(directory + "Sudoku.csv", 27).load(), "sudoku-27");
        models.put(new Sudoku(directory + "Sudoku.csv", 28).load(), "sudoku-28");
        models.put(new Sudoku(directory + "Sudoku.csv", 29).load(), "sudoku-29");
        models.put(new Sudoku(directory + "Sudoku.csv", 30).load(), "sudoku-30");
        models.put(new Sudoku(directory + "Sudoku.csv", 31).load(), "sudoku-31");
        models.put(new Sudoku(directory + "Sudoku.csv", 32).load(), "sudoku-32");
        models.put(new Sudoku(directory + "Sudoku.csv", 33).load(), "sudoku-33");
        models.put(new Sudoku(directory + "Sudoku.csv", 34).load(), "sudoku-34");
        models.put(new Sudoku(directory + "Sudoku.csv", 35).load(), "sudoku-35");
        models.put(new Sudoku(directory + "Sudoku.csv", 36).load(), "sudoku-36");
        models.put(new Sudoku(directory + "Sudoku.csv", 37).load(), "sudoku-37");
        models.put(new Sudoku(directory + "Sudoku.csv", 38).load(), "sudoku-38");
        models.put(new Sudoku(directory + "Sudoku.csv", 39).load(), "sudoku-39");
        models.put(new Sudoku(directory + "Sudoku.csv", 40).load(), "sudoku-40");
        models.put(new Sudoku(directory + "Sudoku.csv", 41).load(), "sudoku-41");
        models.put(new Sudoku(directory + "Sudoku.csv", 42).load(), "sudoku-42");
        models.put(new Sudoku(directory + "Sudoku.csv", 43).load(), "sudoku-43");
        models.put(new Sudoku(directory + "Sudoku.csv", 44).load(), "sudoku-44");
        models.put(new Sudoku(directory + "Sudoku.csv", 45).load(), "sudoku-45");
        models.put(new Sudoku(directory + "Sudoku.csv", 46).load(), "sudoku-46");

        HashMap<VariableHeuristic<Integer>, String> variableHeuristics = new HashMap<>();
        variableHeuristics.put(new InOrderVariable<>(), "In Order Variable");
        variableHeuristics.put(new MostConstrainingVariable<>(), "Most Constraining Variable");
        variableHeuristics.put(new MostConstrainedVariable<>(), "Most Constrained Variable");
        variableHeuristics.put(new RandomOrderVariable<>(), "Random Order Variable");

        HashMap<ValueHeuristic<Integer>, String> valueHeuristics = new HashMap<>();
        valueHeuristics.put(new AscendingValues<>(), "Ascending Values");
        valueHeuristics.put(new LeastConstrainingValue<>(), "Least Constraining Value");


        int progress = 1;

        saverCSV.add("Model");
        saverCSV.add("Algorithm");
        saverCSV.add("Variable Heuristic");
        saverCSV.add("Value Heuristic");
        saverCSV.add("Iterations");
        saverCSV.add("Backtracks");
        saverCSV.add("Time");
        saverCSV.add("Find All Solutions");
        saverCSV.add("Solutions");
        saverCSV.addLastInRow("All Combinations");

        for (Boolean isForwardChecking : algorithms.keySet()) {
            for (Boolean findAllSolutionsFlag : findAllSolutions.keySet()) {
                for (VariableHeuristic<Integer> variableHeuristic : variableHeuristics.keySet()) {
                    for (ValueHeuristic<Integer> valueHeuristic : valueHeuristics.keySet()) {
                        for (Model<Integer> model : models.keySet()) {
                            Model<Integer> modelTemp = (Model<Integer>) Copy.deepCopy(model);
                            Heuristic<Integer> heuristic = new Heuristic<>(modelTemp, valueHeuristic, variableHeuristic);

                            CSP<Integer> csp;
                            if (isForwardChecking) csp = new ForwardChecking<>(modelTemp, heuristic);
                            else csp = new Backtracking<>(modelTemp, heuristic);

                            int allCombinations = findAllSolutions.size();
                            allCombinations *= models.size();
                            allCombinations *= variableHeuristics.size();
                            allCombinations *= valueHeuristics.size();
                            allCombinations *= algorithms.size();


                            System.out.println(String.format("Progress %d/%d", progress, allCombinations));
                            System.out.println("Model: " + models.get(model));

                            csp.setFindAllSolutions(findAllSolutionsFlag);
                            csp.run();
                            System.out.println("Algorithm: " + algorithms.get(isForwardChecking));
                            System.out.println("Variable Heuristic: " + variableHeuristics.get(variableHeuristic));
                            System.out.println("Value Heuristic: " + valueHeuristics.get(valueHeuristic));
                            System.out.println("Iterations: " + csp.getIterations());
                            System.out.println("Backtracks: " + csp.getBacktracks());
                            System.out.println(String.format("Time: %f", csp.getTime()));
                            System.out.println("Find All Solutions: " + findAllSolutions.get(findAllSolutionsFlag));
                            System.out.println("Solutions: " + csp.getSolutions().size());
                            System.out.println(String.format("All Combinations: %.0f", csp.countAllCombinations()));
                            System.out.println();

                            saverCSV.add(models.get(model));
                            saverCSV.add(algorithms.get(isForwardChecking));
                            saverCSV.add(variableHeuristics.get(variableHeuristic));
                            saverCSV.add(valueHeuristics.get(valueHeuristic));
                            saverCSV.add(String.valueOf(csp.getIterations()));
                            saverCSV.add(String.valueOf(csp.getBacktracks()));
                            saverCSV.add(String.format("%f", csp.getTime()));
                            saverCSV.add(findAllSolutions.get(findAllSolutionsFlag));
                            saverCSV.add(String.valueOf(csp.getSolutions().size()));
                            saverCSV.addLastInRow(String.format("%.0f", csp.countAllCombinations()));

                            progress += 1;
                        }
                    }
                }
            }
        }

        saverCSV.save();
    }
}
