import csp.Backtracking;
import csp.CSP;
import csp.ForwardChecking;
import heuristic.Heuristic;
import heuristic.value.AscendingValues;
import heuristic.value.LeastConstrainingValue;
import heuristic.value.ValueHeuristic;
import heuristic.variable.*;
import model.Jolka;
import model.Model;
import model.Sudoku;
import utils.Copy;
import utils.SaverCSV;

import java.util.HashMap;

public class RunAndSaveJolka {
    public static void main(String[] args) {

        String directory = "src/data/";

        SaverCSV saverCSV = new SaverCSV("datasheets/sudoku-results-fast.csv");

        HashMap<Boolean, String> findAllSolutions = new HashMap<>();
//        findAllSolutions.put(true, "true");
        findAllSolutions.put(false, "false");

        HashMap<Boolean, String> algorithms = new HashMap<>();
        algorithms.put(true, "Forward Checking");
        algorithms.put(false, "Backtracking");

        HashMap<Model<String>, String> models = new HashMap<>();
        models.put(new Jolka(directory + "jolka0").load(), "jolka-0");
        models.put(new Jolka(directory + "jolka1").load(), "jolka-1");
        models.put(new Jolka(directory + "jolka2").load(), "jolka-2");
        models.put(new Jolka(directory + "jolka3").load(), "jolka-3");
        models.put(new Jolka(directory + "jolka4").load(), "jolka-4");

        HashMap<VariableHeuristic<String>, String> variableHeuristics = new HashMap<>();
//        variableHeuristics.put(new InOrderVariable<>(), "In Order Variable");
        variableHeuristics.put(new MostConstrainingVariable<>(), "Most Constraining Variable");
        variableHeuristics.put(new MostConstrainedVariable<>(), "Most Constrained Variable");
        variableHeuristics.put(new RandomOrderVariable<>(), "Random Order Variable");

        HashMap<ValueHeuristic<String>, String> valueHeuristics = new HashMap<>();
        valueHeuristics.put(new AscendingValues<>(), "Ascending Values");
        valueHeuristics.put(new LeastConstrainingValue<>(), "Least Constraining Value");


        int progress = 1;

        saverCSV.add("Model");
        saverCSV.add("Algorithm");
        saverCSV.add("Variable Heuristic");
        saverCSV.add("Value Heuristic");
        saverCSV.add("Iterations");
        saverCSV.add("Backtracks");
        saverCSV.add("Time");
        saverCSV.add("Find All Solutions");
        saverCSV.add("Solutions");
        saverCSV.addLastInRow("All Combinations");

        for (Boolean isForwardChecking : algorithms.keySet()) {
            for (Boolean findAllSolutionsFlag : findAllSolutions.keySet()) {
                for (VariableHeuristic<String> variableHeuristic : variableHeuristics.keySet()) {
                    for (ValueHeuristic<String> valueHeuristic : valueHeuristics.keySet()) {
                        for (Model<String> model : models.keySet()) {
                            Model<String> modelTemp = (Model<String>) Copy.deepCopy(model);
                            Heuristic<String> heuristic = new Heuristic<>(modelTemp, valueHeuristic, variableHeuristic);

                            CSP<String> csp;
                            if (isForwardChecking) csp = new ForwardChecking<>(modelTemp, heuristic);
                            else csp = new Backtracking<>(modelTemp, heuristic);

                            int allCombinations = findAllSolutions.size();
                            allCombinations *= models.size();
                            allCombinations *= variableHeuristics.size();
                            allCombinations *= valueHeuristics.size();
                            allCombinations *= algorithms.size();


                            System.out.println(String.format("Progress %d/%d", progress, allCombinations));
                            System.out.println("Model: " + models.get(model));

                            csp.setFindAllSolutions(findAllSolutionsFlag);
                            csp.run();
                            System.out.println("Algorithm: " + algorithms.get(isForwardChecking));
                            System.out.println("Variable Heuristic: " + variableHeuristics.get(variableHeuristic));
                            System.out.println("Value Heuristic: " + valueHeuristics.get(valueHeuristic));
                            System.out.println("Iterations: " + csp.getIterations());
                            System.out.println("Backtracks: " + csp.getBacktracks());
                            System.out.println(String.format("Time: %f", csp.getTime()));
                            System.out.println("Find All Solutions: " + findAllSolutions.get(findAllSolutionsFlag));
                            System.out.println("Solutions: " + csp.getSolutions().size());
                            System.out.println(String.format("All Combinations: %.0f", csp.countAllCombinations()));
                            System.out.println();

                            saverCSV.add(models.get(model));
                            saverCSV.add(algorithms.get(isForwardChecking));
                            saverCSV.add(variableHeuristics.get(variableHeuristic));
                            saverCSV.add(valueHeuristics.get(valueHeuristic));
                            saverCSV.add(String.valueOf(csp.getIterations()));
                            saverCSV.add(String.valueOf(csp.getBacktracks()));
                            saverCSV.add(String.format("%f", csp.getTime()));
                            saverCSV.add(findAllSolutions.get(findAllSolutionsFlag));
                            saverCSV.add(String.valueOf(csp.getSolutions().size()));
                            saverCSV.addLastInRow(String.format("%.0f", csp.countAllCombinations()));

                            progress += 1;
                        }
                    }
                }
            }
        }

        saverCSV.save();
    }
}
