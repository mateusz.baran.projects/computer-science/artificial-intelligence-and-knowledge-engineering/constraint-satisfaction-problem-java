package csp;

import heuristic.Heuristic;
import model.Model;
import model.variable.Variable;


public class Backtracking<T> extends CSP<T> {
    public Backtracking(Model<T> model, Heuristic<T> heuristic) {
        super(model, heuristic);
    }

    protected boolean algorithm(int index) {
        iterations += 1;

        if (index == variables.size()) {
            addSolution();
            return true;
        }

        Variable<T> variable = heuristic.getVariable(index);

        for (T value : heuristic.getSortedDomainValues(variable)) {
            variable.setValue(value);
            if (model.validate(variable)
                    && algorithm(index + 1)
                    && !findAllSolutions)
                return true;
        }

        backtracks += 1;

        variable.resetDomain();
        return false;
    }
}
