package csp;

import heuristic.Heuristic;
import model.Model;
import model.constraint.Constraint;
import model.variable.Variable;
import utils.Copy;

import java.io.Serializable;
import java.util.*;


public abstract class CSP<T> implements Serializable {
    protected Model<T> model;
    protected T nullValue;
    protected HashMap<Variable<T>, List<Constraint>> constraints;
    protected HashMap<Variable<T>, Set<Variable<T>>> constrainedVariables;
    protected List<Variable<T>> variables;

    protected Heuristic<T> heuristic;

    protected Long start;
    protected Long time;
    protected Long iterations;
    protected Long backtracks;
    protected List<CSP<T>> solutions;
    protected boolean findAllSolutions;


    public CSP(Model<T> model, Heuristic<T> heuristic) {
        this.model = model;
        this.nullValue = model.getNullValue();
        this.constraints = model.getConstraints();

        this.constrainedVariables = new HashMap<>();
        for (Variable<T> variable : model.getConstrainedVariables().keySet())
            constrainedVariables.put(variable, new HashSet<>(model.getConstrainedVariables().get(variable)));

        this.variables = heuristic.getVariables();

        this.heuristic = heuristic;

        this.start = 0L;
        this.time = 0L;
        this.backtracks = 0L;
        this.iterations = 0L;
        this.solutions = new ArrayList<>();
        this.findAllSolutions = false;
    }

    public double getTime() {
        return (double) time / 1_000_000_000.0;
    }

    public Long getIterations() {
        return iterations;
    }

    public Long getBacktracks() {
        return backtracks;
    }

    public double countAllCombinations() {
        double combinations = 1;
        for (Variable<T> variable : variables)
            combinations *= variable.getDomainValues().size();

        return combinations;
    }

    public void setFindAllSolutions(boolean findAllSolutions) {
        this.findAllSolutions = findAllSolutions;
    }

    public List<CSP<T>> getSolutions() {
        return solutions;
    }

    protected void addSolution() {
        CSP<T> csp = (CSP<T>) Copy.deepCopy(this);
        Objects.requireNonNull(csp).time = System.nanoTime() - start;
        Objects.requireNonNull(csp).solutions.add(csp);
        solutions.add(csp);
    }

    public Model<T> getModel() {
        return model;
    }

    public void run() {
        start = System.nanoTime();
        algorithm(0);
        time = System.nanoTime() - start;
    }

    protected abstract boolean algorithm(int index);
}
