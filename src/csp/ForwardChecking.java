package csp;

import heuristic.Heuristic;
import model.Model;
import model.variable.Variable;


public class ForwardChecking<T> extends CSP<T> {
    public ForwardChecking(Model<T> model, Heuristic<T> heuristic) {
        super(model, heuristic);
    }

    private boolean validateConstrainedVariables(Variable<T> variable) {
        for (Variable<T> var : constrainedVariables.get(variable))
            if (var.isNull() && !validateDomainValues(var))
                return false;

        return true;
    }

    private boolean validateDomainValues(Variable<T> variable) {
        for (T value : variable.getLeftDomainValues()) {
            variable.setValue(value);
            if (model.validate(variable)) {
                variable.resetDomain();
                return true;
            }
        }

        variable.resetDomain();
        return false;
    }

    protected boolean algorithm(int index) {
        iterations += 1;

        if (index == variables.size()) {
            addSolution();
            return true;
        }

        Variable<T> variable = heuristic.getVariable(index);

        for (T value : heuristic.getSortedDomainValues(variable)) {
            variable.setValue(value);
            if (model.validate(variable)
                    && validateConstrainedVariables(variable)
                    && algorithm(index + 1)
                    && !findAllSolutions)
                return true;
        }

        backtracks += 1;

        variable.resetDomain();
        return false;
    }
}
